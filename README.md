# berry dotfiles



## Preview

## clean
![light](/preview/Screenshot_1600x900.png)
<br />
## dirty
![light](/preview/Screenshot2_1600x900.png)
<br />

## Fonts

- **JetBrainsMono**
- **Font Awesome 5 Free**
- **JetBrainsMono Nerd Font**
- **FontAwesome**
- **Noto Sans Mono CJK JP**

## Details
- **Distro** ubuntu ;)
- **WM** berry
- **Panel** Polybar
- **Terminal** Alacritty
- **Music Player** mpd, ncmpcpp, mpc,
- **Text Editor** vim
- **File Manager** Ranger, with w3m image previewer
- **Alternative File Manager** Thunar
- **Screen Recorder** simplescreenrecorder
- **Program Launcher** rofi
- **Info Fetcher** Neofetch
- **GTK Theme** Orchis
- **Icons** Tela-circle
- **CLI Shell Intepreter** zsh
- **Compositor** picom
- **Notification Daemon** dunst




- ** Special Thanks
- ** adi1090x: [Configuration : berry](https://github.com/archcraft-os/archcraft-berry.git)

