--Place this file in your .xmonad/lib directory and import module Colors into .xmonad/xmonad.hs config
--The easy way is to create a soft link from this file to the file in .xmonad/lib using ln -s
--Then recompile and restart xmonad.

module Colors
    ( wallpaper
    , background, foreground, cursor
    , color0, color1, color2, color3, color4, color5, color6, color7
    , color8, color9, color10, color11, color12, color13, color14, color15
    ) where

-- Shell variables
-- Generated by 'wal'
wallpaper="/home/uriel/.config/berry/wallpapers/red-hair.jpg"

-- Special
background="#2e2531"
foreground="#d6d6d7"
cursor="#d6d6d7"

-- Colors
color0="#2e2531"
color1="#98847E"
color2="#927C81"
color3="#939089"
color4="#AA938C"
color5="#AEA49A"
color6="#B9B4B1"
color7="#d6d6d7"
color8="#959596"
color9="#98847E"
color10="#927C81"
color11="#939089"
color12="#AA938C"
color13="#AEA49A"
color14="#B9B4B1"
color15="#d6d6d7"
