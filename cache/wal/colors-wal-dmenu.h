static const char *colors[SchemeLast][2] = {
	/*     fg         bg       */
	[SchemeNorm] = { "#d6d6d7", "#2e2531" },
	[SchemeSel] = { "#d6d6d7", "#98847E" },
	[SchemeOut] = { "#d6d6d7", "#B9B4B1" },
};
