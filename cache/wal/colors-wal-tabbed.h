static const char* selbgcolor   = "#2e2531";
static const char* selfgcolor   = "#d6d6d7";
static const char* normbgcolor  = "#927C81";
static const char* normfgcolor  = "#d6d6d7";
static const char* urgbgcolor   = "#98847E";
static const char* urgfgcolor   = "#d6d6d7";
