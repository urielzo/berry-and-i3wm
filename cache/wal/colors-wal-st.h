const char *colorname[] = {

  /* 8 normal colors */
  [0] = "#2e2531", /* black   */
  [1] = "#98847E", /* red     */
  [2] = "#927C81", /* green   */
  [3] = "#939089", /* yellow  */
  [4] = "#AA938C", /* blue    */
  [5] = "#AEA49A", /* magenta */
  [6] = "#B9B4B1", /* cyan    */
  [7] = "#d6d6d7", /* white   */

  /* 8 bright colors */
  [8]  = "#959596",  /* black   */
  [9]  = "#98847E",  /* red     */
  [10] = "#927C81", /* green   */
  [11] = "#939089", /* yellow  */
  [12] = "#AA938C", /* blue    */
  [13] = "#AEA49A", /* magenta */
  [14] = "#B9B4B1", /* cyan    */
  [15] = "#d6d6d7", /* white   */

  /* special colors */
  [256] = "#2e2531", /* background */
  [257] = "#d6d6d7", /* foreground */
  [258] = "#d6d6d7",     /* cursor */
};

/* Default colors (colorname index)
 * foreground, background, cursor */
 unsigned int defaultbg = 0;
 unsigned int defaultfg = 257;
 unsigned int defaultcs = 258;
 unsigned int defaultrcs= 258;
